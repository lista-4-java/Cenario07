package br.edu.up.models;

public class Aluno {

    private String nome;
    private int rg;
    private int matricola;
    private int anoIngresso;
    private String nomeCurso;
    private char turno;
    private int feito = 0, feito2 = 0;
    private String situacao;

    public Aluno(String nome, int rg, int matricola, int anoIngresso, String nomeCurso, char turno) {
        this.nome = nome;
        this.rg = rg;
        this.matricola = matricola;
        this.anoIngresso = anoIngresso;
        this.nomeCurso = nomeCurso;
        this.turno = turno;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getRg() {
        return rg;
    }

    public void setRg(int rg) {
        this.rg = rg;
    }

    public int getMatricola() {
        return matricola;
    }

    public void setMatricola(int matricola) {
        this.matricola = matricola;
    }

    public int getAnoIngresso() {
        return anoIngresso;
    }

    public void setAnoIngresso(int anoIngresso) {
        this.anoIngresso = anoIngresso;
    }

    public String getNomeCurso() {
        return nomeCurso;
    }

    public void setNomeCurso(String nomeCurso) {
        this.nomeCurso = nomeCurso;
    }

    public char getTurno() {
        return turno;
    }

    public void setTurno(char turno) {
        this.turno = turno;
    }

    public void verificarCompetenciaNescessaria(int aux2) {

            switch (aux2) {
                case 1:
                    feito = 25;
                    break;

                case 2:
                    feito = 50;
                    break;

                case 3:
                    feito = 75;
                    break;

                case 4:
                    feito = 100;
                    break;

                default:
                    break;
          }
        }

        public void verificarCompetenciaComplementar(int aux3){
            switch (aux3) {
                case 1:
                    feito2 = 25;
                    break;
            
                case 2:
                    feito2 = 50;
                    break;
            
                case 3:
                    feito2 = 75;
                    break;
            
                case 4:
                    feito2 = 100;
                    break;
            
                default:
                    break;
            }
        }



    public void alunoSituacao() {
        if (feito >= 50 && feito2 == 100) {
            situacao = "aprovado";
        } else if (feito < 50 && feito2 < 50) {
            situacao = "reprovado";
        } else {
            situacao = "Pendente";
        }
    }

    @Override
    public String toString() {
        return "Aluno [nome=" + nome + ", rg=" + rg + ", matricola=" + matricola + ", anoIngresso=" + anoIngresso
                + ", nomeCurso=" + nomeCurso + ", turno=" + turno + ", competencia nescessaria =" + feito
                + ", Competencia Complementar=" + feito2
                + ", situacao=" + situacao + "]";
    }
}