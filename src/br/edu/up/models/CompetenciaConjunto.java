package br.edu.up.models;

public class CompetenciaConjunto {
    private String competencia1;
    private String competencia2;
    private String competencia3;
    private String competencia4;
    private boolean classificacao;

    public CompetenciaConjunto(String competencia1, String competencia2, String competencia3, String competencia4, boolean classificacao) {
        this.competencia1 = competencia1;
        this.competencia2 = competencia2;
        this.competencia3 = competencia3;
        this.competencia4 = competencia4;
        this.classificacao = classificacao;
    }

    

    public String getCompetencia1() {
        return competencia1;
    }



    public void setCompetencia1(String competencia1) {
        this.competencia1 = competencia1;
    }



    public String getCompetencia2() {
        return competencia2;
    }



    public void setCompetencia2(String competencia2) {
        this.competencia2 = competencia2;
    }



    public String getCompetencia3() {
        return competencia3;
    }



    public void setCompetencia3(String competencia3) {
        this.competencia3 = competencia3;
    }



    public String getCompetencia4() {
        return competencia4;
    }



    public void setCompetencia4(String competencia4) {
        this.competencia4 = competencia4;
    }



    @Override
    public String toString() {
        return "CompetenciaConjunto [competencia1=" + competencia1 + ", competencia2=" + competencia2
                + ", competencia3=" + competencia3 + ", competencia4=" + competencia4 + ", Nescessaria = " + classificacao + "]";
    }

}
