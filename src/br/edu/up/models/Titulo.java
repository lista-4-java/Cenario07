package br.edu.up.models;

public class Titulo {
    private int numeroIdendificacao;
     private String nomeInstituicao;
     private int anoConclusao;
     private String nomeTitulo;
     private String tituloConclusao;

    public Titulo(int numeroIdendificacao, String nomeInstituicao, int anoConclusao, String nomeTitulo,String tituloConclusao) {
        this.numeroIdendificacao = numeroIdendificacao;
        this.nomeInstituicao = nomeInstituicao;
        this.anoConclusao = anoConclusao;
        this.nomeTitulo = nomeTitulo;
        this.tituloConclusao = tituloConclusao;
    }

    public int getNumeroIdendificacao() {
        return numeroIdendificacao;
    }

    public void setNumeroIdendificacao(int numeroIdendificacao) {
        this.numeroIdendificacao = numeroIdendificacao;
    }

    public String getNomeInstituicao() {
        return nomeInstituicao;
    }

    public void setNomeInstituicao(String nomeInstituicao) {
        this.nomeInstituicao = nomeInstituicao;
    }

    public int getAnoConclusao() {
        return anoConclusao;
    }

    public void setAnoConclusao(int anoConclusao) {
        this.anoConclusao = anoConclusao;
    }

    public String getNomeTitulo() {
        return nomeTitulo;
    }

    public void setNomeTitulo(String nomeTitulo) {
        this.nomeTitulo = nomeTitulo;
    }

    public String getTituloConclusao() {
        return tituloConclusao;
    }

    public void setTituloConclusao(String tituloConclusao) {
        this.tituloConclusao = tituloConclusao;
    }

    @Override
    public String toString() {
        return "Titulo [numeroIdendificacao=" + numeroIdendificacao + ", nomeInstituicao=" + nomeInstituicao
                + ", anoConclusao=" + anoConclusao + ", nomeTitulo=" + nomeTitulo + ", tituloConclusao="
                + tituloConclusao + "]";
    }

}
