package br.edu.up.models;

public class Professor {
    private String nome;
    private int rg;
    private int matricola;

    public Professor(String nome, int rg, int matricola) {
        this.nome = nome;
        this.rg = rg;
        this.matricola = matricola;
    }

    public Professor() {

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getRg() {
        return rg;
    }

    public void setRg(int rg) {
        this.rg = rg;
    }

    public int getMatricola() {
        return matricola;
    }

    public void setMatricola(int matricola) {
        this.matricola = matricola;
    }
    
    //------------------------------------------------------------------

    @Override
    public String toString() {
        return "Professor [nome=" + nome + ", rg=" + rg + ", matricola=" + matricola + "]";
    }

}
 