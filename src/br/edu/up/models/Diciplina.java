package br.edu.up.models;

public class Diciplina {

    private String nomeDisciplina;
    private int identificador;
    private String curiculo;
    private boolean classificacao;

    public Diciplina(String nomeDisciplina, int identificador, String curiculo) {
        this.nomeDisciplina = nomeDisciplina;
        this.identificador = identificador;
        this.curiculo = curiculo;
    }

    public String getNomeDisciplina() {
        return nomeDisciplina;
    }

    public void setNomeDisciplina(String nomeDisciplina) {
        this.nomeDisciplina = nomeDisciplina;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    public String getCuriculo() {
        return curiculo;
    }

    public void setCuriculo(String curiculo) {
        this.curiculo = curiculo;
    }


    // ---------------------------------------------------------------------

    public String professorDaDisciplina(Professor professor) {
       return professor.getNome();
    }

    public void alunoMatriculado(Aluno aluno) {
        aluno.getNome();
    }

    public void setDisciplinaClassificacao(boolean classificacao) {
        this.classificacao = classificacao;
    }

    public boolean getDisciplinaClassificacao(){
        return classificacao;
    }


    @Override
    public String toString() {
        return "Diciplina [nomeDisciplina: " + nomeDisciplina + ", identificador: " + identificador + ", curiculo: "
                + curiculo + ", classificacao: " + classificacao + "]";
    }


    public String DiciplinaStatus(Professor professor, CompetenciaConjunto comp) {
        StringBuilder sb = new StringBuilder();
        
        sb.append(professorDaDisciplina(professor));
        sb.append("\n"); 
        
  
        sb.append(comp.toString());
        sb.append("\n"); 
        
        sb.append(this.toString());
        
        return sb.toString();
    }

    public String listarAlunos(Aluno[] aluno){

        StringBuilder sb = new StringBuilder();
        
        for (int i = 0; i < aluno.length; i++) {

            sb.append(aluno[i].toString());
            sb.append("\n"); 
            
        }
        
        return sb.toString();
        
    }

}
// Todas as disciplinas possuem um
// nome, identificador, currículo a que pertencem e um conjunto de competências,
// classificadas como
// Necessárias e Complementares. Na disciplina também estão registrados o
// professor que a ministra e os
// alunos nela matriculados.