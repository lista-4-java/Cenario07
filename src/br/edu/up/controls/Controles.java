package br.edu.up.controls;

import br.edu.up.models.*;


public class Controles {

    private int qtde = 0;

    private Professor[] professores = new Professor[2];
    private Titulo[] titulos = new Titulo[2];
    private Aluno[] alunos = new Aluno[3];
    private Diciplina[] diciplinas = new Diciplina[2];
    private CompetenciaConjunto[] competencias = new CompetenciaConjunto[2];

    public Controles() {

    }

    public void novoProfessor() {
        professores[0] = new Professor("Pedro", 1546748, 4564654);
        professores[1] = new Professor("joao", 23456464, 79871544);
    }

    public void novoAluno(String nome, int rg, int matricola, int anoIngresso, String nomeCurso, char turno, int aux2, int aux3) {

        alunos[qtde] = new Aluno(nome, rg, matricola, anoIngresso, nomeCurso, turno);
        alunos[qtde].verificarCompetenciaNescessaria(aux2);
        alunos[qtde].verificarCompetenciaComplementar(aux3);
        alunos[qtde].alunoSituacao();
        qtde++;
    }

    public void novoTitulo() {

        titulos[0] = new Titulo(454515445, "UP", 2021, "Titulo1", "2023");
        titulos[1] = new Titulo(45465147, "UP", 2015, "Titulo2", "2020");
    }

    public void novaDisciplina() {
        diciplinas[0] = new Diciplina("Topicos de sistemas", 45645647, "Senior em cs");
        diciplinas[0].setDisciplinaClassificacao(false);
        diciplinas[1] = new Diciplina("Desenvolvimento de Software", 44554247, "Senior em java ");
        diciplinas[1].setDisciplinaClassificacao(true);
    }

    public void novoConjunto() {
        competencias[0] = new CompetenciaConjunto("competencia1","competencia2","competencia3","competencia4",false);
        competencias[1] = new CompetenciaConjunto("competencia1","competencia2","competencia3","competencia4",true);
    }

    public String statusProfessor() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 2; i++) {
            sb.append(professores[i].toString());
            sb.append("\n");
            sb.append(titulos[i].toString());
            sb.append("\n");
        }
        return sb.toString();
    }

    public String statusAluno() {

        StringBuilder sb = new StringBuilder();
        sb.append(alunos[0].toString());
        sb.append("\n");
        sb.append(alunos[1].toString());
        sb.append("\n");
        sb.append(alunos[2].toString());
        sb.append("\n");

        return sb.toString();
    }

    public String statusDisciplina() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 2; i++) {
            sb.append(diciplinas[i].DiciplinaStatus(professores[i], competencias[i]));
            sb.append("\n");
            sb.append(diciplinas[i].listarAlunos(alunos));
            sb.append("\n");
        }
        return sb.toString();
    }

}
