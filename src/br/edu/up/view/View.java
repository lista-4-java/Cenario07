package br.edu.up.view;

import br.edu.up.controls.Controles;
import br.edu.up.utilits.Prompt;

public class View {
    private int aux2 , aux3;


    Controles controle = new Controles();

    public void criarRestante(){
        controle.novoProfessor();
        controle.novoTitulo();
        controle.novaDisciplina();
        controle.novoTitulo();
        controle.novoConjunto();
    }

    public void msg1() {

        Prompt.imprimir("------------CADRASTRO---------------");
        Prompt.imprimir("");
        Prompt.imprimir("1 - cadastrar aluno(cadastrar os 3 alunos)");
        Prompt.imprimir("2 - sair");
    }

    public void cadastrar() {
        int aux = Prompt.lerInteiro();

        switch (aux) {

            case 1:
                String nome = Prompt.lerLinha("digite seu nome: ");
                int rg = Prompt.lerInteiro("digite seu rg: ");
                int matricola = Prompt.lerInteiro("digite sua matricola: ");
                int anoIngresso = Prompt.lerInteiro("digite o ano que foi ingressado: ");
                String nomeCurso = Prompt.lerLinha("digite o nome do curso cursado: ");
                char turno = Prompt.lerCaractere("digite o turno (M / T/ N)");
                aux2 = Prompt.lerInteiro("digite a quantidade de competencias nescessarias feitas (de 1 a 4): ");
                aux3 = Prompt.lerInteiro("digite a quantidade de competencias complementares feitas (de 1 a 4): ");
                
                controle.novoAluno(nome, rg, matricola, anoIngresso, nomeCurso, turno,aux2,aux3);

                Prompt.imprimir("------------CADRASTRADO COM SUCESSO---------------");
                msg1();
                cadastrar();
                break;
                

            default:

                break;
        }
    }

    public void msg2() {
        Prompt.imprimir("----------------------VER STATUS----------------------");
        Prompt.imprimir("");
        Prompt.imprimir("1 - ver status do professor");
        Prompt.imprimir("2 - ver status dos aluno");
        Prompt.imprimir("3 - ver status da disciplina");
        Prompt.imprimir("4 - sair");
    }

    public void controlarStatus() {
        int aux4 = Prompt.lerInteiro();

        switch (aux4) {
            case 1:
                Prompt.imprimir("------------PROFESSORES CADRASTRADOS---------------");
                Prompt.imprimir(controle.statusProfessor());
                msg2();
                controlarStatus();

                break;
            case 2:
                Prompt.imprimir("------------ALUNOS CADRASTRADOS---------------");
                Prompt.imprimir(controle.statusAluno());
                msg2();
                controlarStatus();
                break;
            case 3:
                Prompt.imprimir("------------DISCIPLINAS CADRASTRADAS---------------");
                Prompt.imprimir(controle.statusDisciplina());
                msg2();
                controlarStatus();
                break;

            default:
                break;
        }

    }
}