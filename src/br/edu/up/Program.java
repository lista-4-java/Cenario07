package br.edu.up;

import br.edu.up.view.*;

public class Program {

    public static void main(String[] args) {

        View view = new View();
           
        view.criarRestante();
        view.msg1();
        view.cadastrar();
        view.msg2();
        view.controlarStatus();
    }
    
}
